package fr.innovantic.sandbox;

import android.support.multidex.MultiDexApplication;

import org.androidannotations.annotations.EApplication;

@EApplication
public class Application extends MultiDexApplication
{

    @Override
    public void onCreate()
    {
        super.onCreate();
    }
}
