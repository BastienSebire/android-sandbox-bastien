package fr.innovantic.sandbox.ui.misc;

import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import fr.innovantic.sandbox.utils.Utils;
import fr.innovantic.tools.ui.InnovantoolsActivity;

@EActivity
public abstract class AbstractActivity extends InnovantoolsActivity
{
    protected Snackbar snackbar;

    @ViewById(resName = "coordinatorLayout")
    protected CoordinatorLayout coordinatorLayout;

    /**
     * Affichage d'une snackbar simple.
     * @param text à afficher.
     * @param duration durée de l'affichage de la snackbar. Voir Snackbar.LENGTH.
     * @param showImmediatly affichage après configuration.
     */
    public void prepareSnackbar(String text, @Nullable Integer duration, boolean showImmediatly)
    {
        if(coordinatorLayout != null && !Utils.isBlank(text)) {
            snackbar = Snackbar.make(coordinatorLayout, text, duration == null ? Snackbar.LENGTH_SHORT : duration);
            if(showImmediatly) showSnackbar();
        }

    }

    /**
     * Une snackbar avec une action.
     * @param text à afficher.
     * @param duration durée de l'affichage de la snackbar. Voir Snackbar.LENGTH.
     * @param actionTitle titre de l'action.
     * @param listener listener de l'action.
     * @param showImmediatly affichage après configuration.
     */
    public void prepareSnackbarWithAction(String text, @Nullable Integer duration, String actionTitle, View.OnClickListener listener, boolean showImmediatly)
    {
        prepareSnackbar(text, duration, false);
        if(coordinatorLayout !=null && snackbar != null) {
            snackbar.setAction(actionTitle, listener);
            if(showImmediatly) showSnackbar();
        }
    }

    /**
     * Masque la snackbar affichée.
     */
    public void hideSnackbar()
    {
        if(coordinatorLayout != null && snackbar != null && snackbar.isShown()) {
            snackbar.dismiss();
        }
    }

    /**
     * Affiche la snackbar.
     */
    public void showSnackbar()
    {
        if(coordinatorLayout != null && snackbar != null) {
            snackbar.show();
        }
    }
}
