package fr.innovantic.sandbox.ui;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.Window;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.InstanceState;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;

import fr.innovantic.sandbox.R;
import fr.innovantic.tools.ui.HierarchyUtils;
import fr.innovantic.tools.ui.InnovantoolsDialogFragment;
import timber.log.Timber;

/**
 * Dialog fragment contenant une webview avec loader.
 */
@EFragment(R.layout.webview_dialog_fragment)
public class WebviewDialogFragment extends InnovantoolsDialogFragment
{

    public interface WebviewDialogFragmentListener
    {
        void onWebResourceError(WebResourceError error);

        void onUrlIntercepted(String url);
    }

    @ViewById(R.id.progressBar)
    protected ProgressBar mProgressBar;

    @InstanceState
    @FragmentArg
    protected String mUrl;

    @InstanceState
    @FragmentArg
    protected ArrayList<String> mInterceptor;

    @ViewById(R.id.webview)
    protected WebView mWebview;



    /**
     * @return une instance de cet écran.
     */
    public static WebviewDialogFragment create(final String url, @Nullable ArrayList<String> interceptor)
    {

        return WebviewDialogFragment_.builder()
                .mUrl(url)
                .mInterceptor(interceptor)
                .build();
    }

    @SuppressLint("SetJavaScriptEnabled")
    @AfterViews
    protected void init()
    {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(null);
        getDialog().setCanceledOnTouchOutside(false);
        getDialog().setCancelable(true);

        mWebview.setWebViewClient(new WebViewDialogClient());
        mWebview.getSettings().setJavaScriptEnabled(true);
        mWebview.getSettings().setBuiltInZoomControls(true);
        mWebview.getSettings().setUseWideViewPort(true);
        mWebview.setInitialScale(1);

        mWebview.loadUrl(mUrl);
    }

    public void show(FragmentActivity activity)
    {
        if (activity.getCurrentFocus() != null) {
            activity.getCurrentFocus().clearFocus();
        }

        try {
            activity.getSupportFragmentManager().executePendingTransactions();
        } catch (Throwable err) {
            Timber.e("Error while executing pending transactions.");
        }

        FragmentTransaction ft = activity.getSupportFragmentManager().beginTransaction();
        ft.add(this, "WebviewDialogFragment");
        ft.commitAllowingStateLoss();
    }

    public WebView getWebview()
    {
        return mWebview;
    }

    /**
     * Web client custom.
     */
    private class WebViewDialogClient extends WebViewClient
    {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon)
        {
            super.onPageStarted(view, url, favicon);
            if (isAdded()) mProgressBar.setVisibility(View.VISIBLE);
        }

        @Override
        public void onPageFinished(WebView view, String url)
        {
            super.onPageFinished(view, url);
            if (isAdded()) mProgressBar.setVisibility(View.GONE);
        }

        /**
         * Interception des urls.
         */
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url)
        {
            final WebviewDialogFragmentListener listener =
                    HierarchyUtils.getParent(mFragment, WebviewDialogFragmentListener.class);

            if (listener != null && mInterceptor != null && mInterceptor.size() > 0) {
                for (String part : mInterceptor) {
                    if (url.contains(part)) {
                        listener.onUrlIntercepted(url);
                        dismiss();
                        return true;
                    }
                }
            }

            return super.shouldOverrideUrlLoading(view, url);
        }

        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error)
        {
            super.onReceivedError(view, request, error);
            final WebviewDialogFragmentListener listener =
                    HierarchyUtils.getParent(mFragment, WebviewDialogFragmentListener.class);

            if (listener != null) listener.onWebResourceError(error);
            dismiss();
        }
    }
}