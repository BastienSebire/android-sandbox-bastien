package fr.innovantic.sandbox.ui.views;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.annotation.StringRes;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import fr.innovantic.sandbox.R;

/**
 * Un bouton arrondi capable d'afficher une ProgressBar.
 */
public class ProgressButton extends FrameLayout
{

    protected TextView button;

    protected ProgressBar progress;

    protected View layout;

    protected String title;

    protected Drawable background;

    protected ColorStateList textColor;

    protected Drawable icon;

    protected float textSize;

    protected int padding;

    protected int progressColor;

    protected int getLayout()
    {
        return R.layout.view_progress_button;
    }

    public ProgressButton(Context context)
    {
        super(context);
        init(context, null);
    }

    public ProgressButton(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        init(context, attrs);
    }

    public ProgressButton(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    @TargetApi(21)
    public ProgressButton(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes)
    {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context, attrs);
    }

    protected void init(Context context, AttributeSet attrs)
    {
        inflate(context, getLayout(), this);
        button = findViewById(R.id.text);
        progress = findViewById(R.id.progress);
        layout = findViewById(R.id.layout);

        progress.setAlpha(0f);

        setAttributes(context, attrs);
    }

    protected void setAttributes(Context context, AttributeSet attrs)
    {
        if (attrs != null) {

            TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.ProgressButton, 0, 0);

            try {
                title = a.getString(R.styleable.ProgressButton_android_text);
                button.setText(title);

                textColor = a.getColorStateList(R.styleable.ProgressButton_android_textColor);
                if (textColor != null) {
                    button.setTextColor(textColor);
                }

                background = a.getDrawable(R.styleable.ProgressButton_background);
                if (background != null) {
                    layout.setBackground(background);
                }

                textSize = a.getDimensionPixelSize(R.styleable.ProgressButton_textSize, 0);
                if (textSize  > 0) {
                    button.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
                }

                padding = a.getDimensionPixelSize(R.styleable.ProgressButton_android_padding, 10);
                layout.setPadding(padding, padding, padding, padding);

                progressColor = a.getColor(R.styleable.ProgressButton_progressColor, getResources().getColor(R.color.colorAccent));
                progress.getIndeterminateDrawable().setColorFilter(progressColor, PorterDuff.Mode.SRC_IN);

            } finally {
                a.recycle();
            }
        }
    }

    public TextView getButton()
    {
        return button;
    }

    public ProgressBar getProgress()
    {
        return progress;
    }

    /**
     * Définit le texte du bouton.
     */
    public void setText(String text)
    {

        button.setText(text);
    }

    public void setText(@StringRes int text)
    {

        button.setText(text);
    }

    /**
     * Affiche/masque la progress bar et verrouille/déverrouille par la même occasion.
     */
    public void setProgressVisible(boolean visible)
    {

        setClickable(!visible);
        if(visible) {
            button.animate().alpha(0f);
            progress.animate().alpha(1.0f);
        } else {
            button.animate().alpha(1f);
            progress.animate().alpha(0f);
        }
    }

    /**
     * @return la visibilité de la progress bar.
     */
    public boolean isProgressVisible()
    {
        return progress.getAlpha() == 1.0f;
    }
}