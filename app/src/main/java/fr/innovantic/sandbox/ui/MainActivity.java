package fr.innovantic.sandbox.ui;

import android.view.Menu;
import android.view.MenuItem;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import fr.innovantic.sandbox.R;
import fr.innovantic.sandbox.ui.misc.AbstractActivity;
import fr.innovantic.sandbox.ui.views.ProgressButton;

/**
 * Activity principale de l'application.
 */
@EActivity(R.layout.main_activity)
public class MainActivity extends AbstractActivity
{
    @ViewById(R.id.button)
    protected ProgressButton button;

    @AfterViews
    protected void init()
    {
        /*prepareSnackbarWithAction("Une snack", Snackbar.LENGTH_INDEFINITE, "Action ! ", v -> {
            Timber.i("TEEEEST");
            snackbar.dismiss();
        }, true);*/

        //Utils.startMapIntentWithReverseGeocoding(this, "5 rue du golf, 33700, Mérignac");
    }

    @Click(R.id.button)
    protected void onButtonClick()
    {
        button.setProgressVisible(!button.isProgressVisible());
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
