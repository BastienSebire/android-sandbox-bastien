package fr.innovantic.sandbox.utils;


import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.provider.CalendarContract;
import android.support.annotation.Nullable;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Calendar;
import java.util.Currency;
import java.util.TimeZone;

public class Utils extends fr.innovantic.tools.Utils
{
    /**
     * Check si un email est valide.
     *
     * @param email à vérifier
     * @return true si l'email est valid.
     */
    public static boolean isEmailValid(CharSequence email)
    {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    /**
     * Lance l'intent d'appel.
     *
     * @param context
     * @param phoneNumber à appeler.
     */
    public static void startCallIntent(Context context, String phoneNumber)
    {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + phoneNumber));
        if (intent.resolveActivity(context.getPackageManager()) != null) {
            context.startActivity(intent);
        }
    }

    /**
     * Lance l'intent de composition de sms.
     *
     * @param context
     * @param phoneNumber auquel envoyer un sms.
     */
    public static void startMessageIntent(Context context, String phoneNumber)
    {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("smsto:" + Uri.encode(phoneNumber)));
        if (intent.resolveActivity(context.getPackageManager()) != null) {
            context.startActivity(intent);
        }
    }

    /**
     * Intent d'envoi de mail.
     *
     * @param context
     * @param mail    auquel envoyer le message.
     * @param subject du mail.
     * @param body    du mail.
     */
    public static void startMailIntent(Context context, String mail, @Nullable String subject, @Nullable String body)
    {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setType("text/plain");
        intent.setData(Uri.parse("mailto:" + mail));

        if (!Utils.isBlank(subject)) intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        if (!Utils.isBlank(body)) intent.putExtra(Intent.EXTRA_TEXT, body);

        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    /**
     * Permet de lancer une application de maps en fournissant l'adresse d'un lieu.
     *
     * @param context
     * @param address à rechercher.
     */
    public static void startMapIntentWithReverseGeocoding(Context context, String address)
    {
        String uri = "geo:0,0?q=" + android.net.Uri.encode(String.format("%s", address), "UTF-8");
        context.startActivity(new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(uri)));
    }

    /**
     * Ouverture de Google Maps avec itinétaire.
     *
     * @param context
     * @param lat       latitude
     * @param lng       longitutde
     * @param placeName nom du lieu.
     */
    public static void startGmapsIntent(Context context, double lat, double lng, @Nullable String placeName)
    {
        String strUri = "http://maps.google.com/maps?q=loc:" + lat + "," + lng + " (" + placeName + ")";
        Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(strUri));
        intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
        context.startActivity(intent);
    }

    /**
     * Retourne un prix formatté avec la monnaie associée.
     * Ex : 12,33 €
     *
     * @param price        à formatter
     * @param currencyUsed
     * @return le prix formatté.
     */
    public static String formatPrice(double price, String currencyUsed)
    {
        String formatedPrice;
        DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance();
        symbols.setGroupingSeparator(' ');

        String pattern = price % 1.0 > 0 ? "###,###.00" : "###,###.#";
        DecimalFormat formatter = new DecimalFormat(pattern, symbols);
        formatedPrice = formatter.format(price) + " " + currencyUsed;

        return formatedPrice;
    }

    /**
     * Permet d'ajouter un événement au calendrier du téléphone.
     *
     * @param context
     * @param eventName     nom de l'événement
     * @param eventLocation lieu de l'événement
     * @param startDate     début de l'événement
     * @param endDate       fin de l'événement
     * @param description   description de l'événement
     * @param allDay        journée complète.
     */
    public static void addEventToCalendar(Context context, String eventName, @Nullable String eventLocation, Calendar startDate, @Nullable Calendar endDate, @Nullable String description, boolean allDay)
    {
        Intent intent = new Intent(Intent.ACTION_INSERT).setData(CalendarContract.Events.CONTENT_URI);

        if (!Utils.isBlank(eventName)) intent.putExtra(CalendarContract.Events.TITLE, eventName);

        if (!Utils.isBlank(eventLocation))
            intent.putExtra(CalendarContract.Events.EVENT_LOCATION, eventLocation);

        if (startDate != null) {
            Calendar finalStartDate = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
            finalStartDate.set(startDate.get(Calendar.YEAR), startDate.get(Calendar.MONTH), startDate.get(Calendar.DAY_OF_MONTH));
            intent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, finalStartDate.getTimeInMillis());
        }

        if (endDate != null) {
            Calendar finalEndDate = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
            finalEndDate.set(endDate.get(Calendar.YEAR), endDate.get(Calendar.MONTH), endDate.get(Calendar.DAY_OF_MONTH));
            intent.putExtra(CalendarContract.EXTRA_EVENT_END_TIME, finalEndDate.getTimeInMillis());
        }

        intent.putExtra(CalendarContract.EXTRA_EVENT_ALL_DAY, allDay);

        if (!Utils.isBlank(description))
            intent.putExtra(CalendarContract.Events.DESCRIPTION, description);

        context.startActivity(intent);
    }
}
